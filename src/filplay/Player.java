package filplay;

import javax.swing.SwingUtilities;
import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.binding.LibVlcFactory;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;

public class Player {

    public static void main(final String[] args) throws Exception {
         LibVlc libVlc = LibVlcFactory.factory().create();
         
        new NativeDiscovery().discover();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new PlayerControl(args);
            }
        });
    }

}
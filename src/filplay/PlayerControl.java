/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filplay;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Dimension;
import javax.swing.ImageIcon;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.binding.LibVlcConst;
import uk.co.caprica.vlcj.filter.swing.SwingFileFilterFactory;

/**
 *
 * @author azzamsya
 */
public class PlayerControl {

    private final JFrame frame;

    private final EmbeddedMediaPlayerComponent mediaPlayerComponent;

    //button
    private final JButton pauseButton;
    private final JButton rewindButton;
    private final JButton skipButton;
    private final JSlider volumeSlider;
    private JFileChooser fileChooser;
    private final JButton chooseButton;
    private final JButton toggleMuteButton;
    private final JButton stopButton;
    private final JButton exitButton;
    private final JButton captureButton;
    private final JButton fastForwardButton;
    private final JButton playButton;
    private final JButton subTitlesButton;
    private final JButton metadataButton;
    private final JButton fullscreenButton;
    private final JSlider zoomSlider;
    private final JButton connectButton;
    private final JLabel zoomLabel;
    private final JLabel volLabel;
    private final JSeparator sep;

    public PlayerControl(String[] args) {

        frame = new JFrame("FilPlay");
        frame.setBounds(100, 100, 600, 400);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                mediaPlayerComponent.release();
                System.exit(0);
            }
        });

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());

        mediaPlayerComponent = new EmbeddedMediaPlayerComponent();
        contentPane.add(mediaPlayerComponent, BorderLayout.CENTER);
        JPanel controlsPane = new JPanel();

        pauseButton = new JButton(new ImageIcon("icons/pause-symbol.png"));
        pauseButton.setToolTipText("Jeda Video");

        rewindButton = new JButton(new ImageIcon("icons/arrowheads-pointing-to-the-left.png"));
        rewindButton.setToolTipText("Rewind Video");

        skipButton = new JButton(new ImageIcon("icons/forward-button.png"));
        skipButton.setToolTipText("Percepat Video");

        //volume slider
        volumeSlider = new JSlider();
        volumeSlider.setOrientation(JSlider.HORIZONTAL);
        volumeSlider.setMinimum(LibVlcConst.MIN_VOLUME);
        volumeSlider.setMaximum(LibVlcConst.MAX_VOLUME);
        volumeSlider.setPreferredSize(new Dimension(100, 40));
        volumeSlider.setToolTipText("Ubah volume");

        //file chooser
        fileChooser = new JFileChooser();
        fileChooser.setApproveButtonText("Putar");
        fileChooser.addChoosableFileFilter(SwingFileFilterFactory.newVideoFileFilter());
        fileChooser.addChoosableFileFilter(SwingFileFilterFactory.newAudioFileFilter());
        fileChooser.addChoosableFileFilter(SwingFileFilterFactory.newPlayListFileFilter());
        FileFilter defaultFilter = SwingFileFilterFactory.newMediaFileFilter();
        fileChooser.addChoosableFileFilter(defaultFilter);
        fileChooser.setFileFilter(defaultFilter);
        //controlsPane.add(fileChooser);

        //chooseButton
        chooseButton = new JButton(new ImageIcon("icons/folder-white-shape.png"));
        //chooseButton.setIcon(new ImageIcon(getClass().getClassLoader().getResource("fa-github")));
        chooseButton.setToolTipText("Pilih Video");

        //mute
        toggleMuteButton = new JButton(new ImageIcon("icons/volume-off-indicator.png"));
        toggleMuteButton.setToolTipText("Bisu Video");

        //stop
        stopButton = new JButton(new ImageIcon("icons/stop-circle.png"));
        stopButton.setToolTipText("Hentikan");

        //exit
        exitButton = new JButton(new ImageIcon("icons/remove-button.png"));
        exitButton.setToolTipText("Keluar");

        //screnshot
        captureButton = new JButton(new ImageIcon("icons/photo-camera.png"));
        captureButton.setToolTipText("Ambil Gambar");

        //fastforward
        fastForwardButton = new JButton(new ImageIcon("icons/fast-forward-arrows.png"));
        fastForwardButton.setToolTipText("Maju kedepan");

        //play
        playButton = new JButton(new ImageIcon("icons/play-sign.png"));
        playButton.setToolTipText("Putar");

        //subtitle
        subTitlesButton = new JButton(new ImageIcon("icons/subtitles.png"));
        subTitlesButton.setToolTipText("Pilih Sub-Title");

        //metadata button
        metadataButton = new JButton(new ImageIcon("icons/rdf-file.png"));
        metadataButton.setToolTipText("Tampilkan Metadata");

        //fulscreenButton
        fullscreenButton = new JButton(new ImageIcon("icons/fullscreen.png"));
        fullscreenButton.setToolTipText("Layar Penuh");
        

        //JOptionPane pane = new JOptionPane();
        UIManager.put("OptionPane.minimumSize", new Dimension(500, 500));

        //zoomSlider
        zoomSlider = new JSlider();
        zoomSlider.setOrientation(JSlider.HORIZONTAL);
        zoomSlider.setMinimum(LibVlcConst.MIN_VOLUME);
        zoomSlider.setMaximum(LibVlcConst.MAX_VOLUME);
        zoomSlider.setPreferredSize(new Dimension(100, 40));
        zoomSlider.setToolTipText("Ubah Volume");

        //connect
        connectButton = new JButton(new ImageIcon("icons/upload.png"));
        connectButton.setToolTipText("Sambungkan Video");

        //label
        zoomLabel = new JLabel("Zoom");
        volLabel = new JLabel("Volume");
        
        //separator
        sep = new JSeparator(SwingConstants.CENTER);
        sep.setBackground(Color.black);
        sep.setForeground(Color.green);

        //add to controls
        //media
        controlsPane.add(chooseButton);
        controlsPane.add(connectButton);
        controlsPane.add(playButton);
        controlsPane.add(skipButton);
        controlsPane.add(pauseButton);
        controlsPane.add(rewindButton);
        controlsPane.add(fastForwardButton);
        controlsPane.add(stopButton);
        //operation
        controlsPane.add(sep);
        controlsPane.add(captureButton);
        controlsPane.add(subTitlesButton);
        controlsPane.add(metadataButton);
        controlsPane.add(fullscreenButton);
        controlsPane.add(exitButton);
        //more
        controlsPane.add(sep);
        controlsPane.add(zoomLabel);
        controlsPane.add(zoomSlider);
        controlsPane.add(toggleMuteButton);
        controlsPane.add(volLabel);
        controlsPane.add(volumeSlider);

        contentPane.add(controlsPane, BorderLayout.SOUTH);
        
       

        pauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().pause();
            }
        });
        rewindButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().skip(-10000);
            }
        });

        skipButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().skip(10000);
            }
        });

        volumeSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                mediaPlayerComponent.getMediaPlayer().setVolume(source.getValue());
            }
        });

        chooseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Component modalToComponent = null;
                if (fileChooser.showOpenDialog(modalToComponent) == JFileChooser.APPROVE_OPTION) {
                    mediaPlayerComponent.getMediaPlayer().playMedia(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        toggleMuteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().mute();
            }
        });

        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().stop();
            }
        });

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().stop();
                System.exit(0);
            }
        });

        captureButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().saveSnapshot();
            }
        });

        fastForwardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().skip(10 * 1000);
            }
        });

        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().play();
            }
        });

        subTitlesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Component modalToComponent = null;
                if (fileChooser.showOpenDialog(modalToComponent) == JFileChooser.APPROVE_OPTION) {
                    mediaPlayerComponent.getMediaPlayer().setSubTitleFile(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }

        });

        metadataButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, mediaPlayerComponent.getMediaPlayer().getVideoDescriptions());
            }

        });

        fullscreenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mediaPlayerComponent.getMediaPlayer().setFullScreen(true);
                mediaPlayerComponent.getMediaPlayer().toggleFullScreen();
            }

        });

        zoomSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                mediaPlayerComponent.getMediaPlayer().setScale(zoomSlider.getValue());
            }
        });

        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //mediaPlayer.enableOverlay(false);
                String mediaUrl = JOptionPane.showInputDialog(null, "Enter a media URL", "Connect to media", JOptionPane.QUESTION_MESSAGE);
                if (mediaUrl != null && mediaUrl.length() > 0) {
                    mediaPlayerComponent.getMediaPlayer().playMedia(mediaUrl);
                }
                mediaPlayerComponent.getMediaPlayer().enableOverlay(true);
            }
        });

        frame.setContentPane(contentPane);
        frame.setVisible(true);

        //mediaPlayerComponent.getMediaPlayer().playMedia("big_buck_bunny.mp4");
    }

}
